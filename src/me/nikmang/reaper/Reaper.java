package me.nikmang.reaper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import me.nikmang.reaper.api.District;
import me.nikmang.reaper.handlers.FileManager;

/**
 * 
 * @author Nikita
 *
 * @since 13 Apr 2016
 */
public class Reaper extends JavaPlugin {
	
	private static Map<Integer, District> districts;
	
	@Override
	public void onEnable() {
		districts = new HashMap<>();
		
		for(int i = 1; i <= 12; i++) {
			districts.put(i, new District(i));
			Bukkit.getPluginManager().addPermission(new Permission("reap.district" + i));
		}
		
		//File stuff
		if(!this.getDataFolder().exists()) {
			this.getDataFolder().mkdirs();
		}
		
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();

		FileManager.getFileManager(this).loadData();
		
		this.getCommand("reap").setExecutor(new MainCommand(this));
		Bukkit.getPluginManager().registerEvents(new OnInteract(Material.valueOf(this.getConfig().getString("tesserae"))), this);
		System.out.println("This is definitely not a virus...");
	}
	
	@Override
	public void onDisable() {
		FileManager.getFileManager(this).saveData();
	}
	
	public static Map<Integer, District> getDistricts() {
		return Collections.unmodifiableMap(districts);
	}
}
