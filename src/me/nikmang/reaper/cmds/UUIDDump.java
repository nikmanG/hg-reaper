package me.nikmang.reaper.cmds;

import java.util.Random;
import java.util.UUID;

import org.bukkit.command.CommandSender;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;

/**
 * 
 * @author Nikita
 *
 * @since 13 Apr 2016
 */
@CommandInfo(aliases = { "ud" }, description = "DO NOT USE", nonPlayer = false, usage = "")
public class UUIDDump extends GenericCommand {

	@Override
	public void onCommand(CommandSender sender, String[] args) {
		Random r = new Random();
		
		for (int i = 0; i < 100; i++) {
			int district = r.nextInt(13);
			
			if(district == 0) {
				continue;
			} else {
				int gender = r.nextInt(2);

				if(gender == 0) {
					Reaper.getDistricts().get(district).addMaleTesserae(UUID.randomUUID(), r.nextInt(50));
				} else {
					Reaper.getDistricts().get(district).addFemaleTesserae(UUID.randomUUID(), r.nextInt(50));
				}
			}
		}
	}

}
