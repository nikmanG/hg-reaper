package me.nikmang.reaper.cmds;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import net.md_5.bungee.api.ChatColor;

@CommandInfo(aliases = { "createlist", "genlist" }, description = "Generates a simple txt list of latest reaping. WARNING: heavy duty activity", nonPlayer = true, usage = "")
public class CreateList extends GenericCommand{

	private Reaper plugin;
	
	public CreateList(Reaper plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void onCommand(CommandSender sender, String[] args) {
		sender.sendMessage(ChatColor.DARK_RED + "##WARNING## " + ChatColor.RED + "Heavy activity");
				
		//Run async to prevent possible bottleneck
		new BukkitRunnable() {
			
			@Override
			public void run() {
				StringBuilder builder = new StringBuilder();
				
				for(String s : GenerateTributes.reapList) {
					builder.append(s + "\n");
				}
				
				try {
					Files.write(Paths.get(plugin.getDataFolder().toString(), "reap.txt"), builder.toString().getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				cancel();
			}
		}.runTaskAsynchronously(plugin);
	}
}
