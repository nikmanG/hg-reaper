package me.nikmang.reaper.cmds;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import org.bukkit.command.CommandSender;

/**
 * Created by ngile on 6/6/2016.
 */
@CommandInfo(description = "Resets the population's Tesserae to 1 each", usage = "", aliases = {"reset", "restart", "r"}, nonPlayer = true)
public class ResetTesserae extends GenericCommand {

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Reaper.getDistricts().values().forEach(d -> {
            d.getMaleTesserae().keySet().forEach(v -> d.setMaleTesserae(v, 1));
            d.getFemaleTesserae().keySet().forEach(v -> d.setFemaleTesserae(v, 1));
        });

        MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.GOOD, "All players reset to 1 tesserae");
    }
}
