package me.nikmang.reaper.cmds;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.FileManager;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by ngile on 6/8/2016.
 */
@CommandInfo(description = "Removes player from their district", usage = "<Player>", aliases = {"removeplayer", "rp"}, nonPlayer = true)
public class RemovePlayer extends GenericCommand {

    private Reaper plugin;

    public RemovePlayer(Reaper plugin) {
       this.plugin = plugin;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if(args.length < 1) {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Need to specify player name");
            return;
        }

        OfflinePlayer op = Bukkit.getOfflinePlayer(args[0]);

        if(ReapAPI.removePlayer(op.getUniqueId())) {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.GOOD, "Successfully removed player from district");
        } else {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "removal of player from district has failed, no district data found");
            return;
        }

        new BukkitRunnable() {

            @Override
            public void run() {
                FileManager.getFileManager(plugin).removePlayer(op.getUniqueId());
            }
        }.runTaskAsynchronously(plugin);
    }
}
