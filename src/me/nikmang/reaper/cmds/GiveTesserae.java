package me.nikmang.reaper.cmds;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import me.nikmang.reaper.handlers.MessageManager.MessageType;
import net.md_5.bungee.api.ChatColor;

/**
 * 
 * @author Nikita
 *
 * @since 13 Apr 2016
 */
@CommandInfo(aliases = { "givetesserae", "gt", "tesserae" }, description = "Gives a player a tesserae object", nonPlayer = true, usage = "<Player>")
public class GiveTesserae extends GenericCommand {

	private ItemStack tesserae;
	
	public GiveTesserae(Reaper plugin) {
		tesserae = new ItemStack(Material.getMaterial(plugin.getConfig().getString("tesserae")));
		ItemMeta meta = tesserae.getItemMeta();
		
		meta.setDisplayName(ChatColor.RED + "[" + ChatColor.GRAY + "Tesserae" + ChatColor.RED + "]");
		
		List<String> lore = new ArrayList<>();
		lore.add(ChatColor.RED + "Right click me to get extra chance");
		
		meta.setLore(lore);

        tesserae.setItemMeta(meta);
	}
	
	@Override
	public void onCommand(CommandSender sender, String[] args) {
		if(args.length < 1) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Insufficient arguments: /reap givetesserae <player>");
			return;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		
		if(target == null) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Player is offline!");
			return;
		}
		
		target.getInventory().addItem(tesserae);
		target.updateInventory();
	}

}
