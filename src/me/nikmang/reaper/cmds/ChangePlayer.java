package me.nikmang.reaper.cmds;

import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by ngile on 6/7/2016.
 */
@CommandInfo(description = "Change player to a different district", usage = "<Player> <New District>", aliases = {"changeplayer", "cp"}, nonPlayer = true)
public class ChangePlayer extends GenericCommand {


    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if(args.length < 2) {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Insufficient args!");
            return;
        }

        Player pl = Bukkit.getPlayer(args[0]);

        if(pl == null) {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Player not online!");
            return;
        }

        District d = ReapAPI.getDistrictByPlayer(pl.getUniqueId());
        int newDistrict = Integer.parseInt(args[1]);

        if(d == null) {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Player does not have a district!");
            return;
        }

        ReapAPI.Gender gender;
        int tesserae = 1;

        //TODO: possibly make this one call
        if(d.getMaleTesserae().containsKey(pl.getUniqueId())) {
            gender = ReapAPI.Gender.MALE;
            tesserae = d.getMaleTesserae().get(pl.getUniqueId());
        } else {
            gender = ReapAPI.Gender.FEMALE;
            tesserae = d.getFemaleTesserae().get(pl.getUniqueId());
        }

        ReapAPI.addPlayer(pl.getUniqueId(), newDistrict, gender, tesserae);
        d.removePlayer(pl.getUniqueId());
    }
}
