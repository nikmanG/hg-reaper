package me.nikmang.reaper.cmds;

import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by ngile on 6/6/2016.
 */
@CommandInfo(description = "[DO NOT USE MANUALLY] Adds Player to valid district as volunteer", usage = "", aliases = {"addme", "setv", "sv", "setvolunteer"}, nonPlayer = false)
public class SetVolunteer extends GenericCommand {

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Player pl = (Player) sender;

        Volunteer.Sacrifice s;
        District d = ReapAPI.getDistrictByPlayer(pl.getUniqueId());

        if(d != null && (s = Volunteer.volunteers.get(d.getNumber())) != null) {
            if(s.addPlayer(pl)) {
                MessageManager.getMessageManager().msg(pl, MessageManager.MessageType.GOOD, "Successfully added as tribute");
            } else {
                MessageManager.getMessageManager().msg(pl, MessageManager.MessageType.BAD, "Not added as tribute");
            }

            return;
        }

        MessageManager.getMessageManager().msg(pl, MessageManager.MessageType.BAD, "Volunteering is not active for your District");
    }
}
