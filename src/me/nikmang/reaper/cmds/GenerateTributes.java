package me.nikmang.reaper.cmds;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.events.PreReapEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import me.nikmang.reaper.handlers.MessageManager.MessageType;

@CommandInfo(aliases = { "generate" }, description = "Generates a list of tributres", nonPlayer = true, usage = "[District] [Gender]")
public class GenerateTributes extends GenericCommand {

	//Saved as UUID_District_Gender
	public static Set<String> reapList;

	private Random r;
	
	{
		r = new Random();
		
		reapList = new HashSet<>();
	}
	
	@Override
	public void onCommand(CommandSender sender, String[] args) {
		PreReapEvent event = new PreReapEvent();

		Bukkit.getPluginManager().callEvent(event);

		if(event.isCancelled()) {
			return;
		}

		if(args.length > 0) {

			try {
				int district = Integer.parseInt(args[0]);

                if(district > 12 || district < 1) {
                    throw new NumberFormatException();
                }

                District d = Reaper.getDistricts().get(district);

				if(args.length > 1) {
				    if(args[1].toUpperCase().equals("MALE")) {
                        getMaleReap(sender, d);
                    } else {
                        getFemaleReap(sender, d);
                    }

                    return;
				} else {
                    getFemaleReap(sender, d);
                    getMaleReap(sender, d);
					return;
				}
			} catch(NumberFormatException e) {
				MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Argument as to be number [1-12]");
				return;
			}
		}

        reapList = new HashSet<>();
		//Random list
		Reaper.getDistricts().entrySet().forEach(e -> {
			//Vars to prevent numerous recalculation of reaplist
			getMaleReap(sender, e.getValue());
            getFemaleReap(sender, e.getValue());
		});

		reapList = Collections.synchronizedSet(reapList);
	}

    //Convenience methods
    private void getMaleReap(CommandSender sender, District d) {
        List<UUID> maleList = d.getMaleReapList();

        if(!maleList.isEmpty()) {
            UUID male = getRandomReap(d.getMaleReapList());
            reapList.add(male + "_" + d.getNumber() + "_" + "MALE");
            MessageManager.getMessageManager().msg(sender, MessageType.INFO, "[MALE] District " + d.getNumber() + ": " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(male).getName());
        }
    }

    private void getFemaleReap(CommandSender sender, District d) {
        List<UUID> femaleList = d.getFemaleReapList();

        if(!femaleList.isEmpty()) {
            UUID female = getRandomReap(d.getFemaleReapList());
            reapList.add(female + "_" + d.getNumber() + "_" + "FEMALE");
            MessageManager.getMessageManager().msg(sender, MessageType.INFO, "[FEMALE] District " + d.getNumber() + ": " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(female).getName());
        }
    }

	private UUID getRandomReap(List<UUID> players) {
		return players.get(r.nextInt(players.size()));
	}
}
