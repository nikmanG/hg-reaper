package me.nikmang.reaper.cmds;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ngile on 6/6/2016.
 */
@CommandInfo(description = "Creates clickable volunteer broadcast", usage = "[district] [gender]", aliases = {"v", "volunteer"}, nonPlayer = true)
public class Volunteer extends GenericCommand {

    public static Map<Integer, Sacrifice> volunteers;
    private Reaper plugin;

    static {
        volunteers = new HashMap<>();
    }

    public Volunteer(Reaper plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        String middle = "=-=-=-=-=";

        if(args.length < 1) {
            for(int i=1; i<=12; i++) {
                volunteers.put(i, new Sacrifice(true, true));
            }
        } else {
            try {
                int dis = Integer.parseInt(args[0]);

                if(args.length > 1) {
                    if(args[1].equalsIgnoreCase("MALE")) {
                        volunteers.put(dis, new Sacrifice(true, false));
                        middle = "=-=[" + ChatColor.DARK_PURPLE + dis + " MALE" + ChatColor.YELLOW + " ]=-=";
                    } else {
                        volunteers.put(dis, new Sacrifice(false, true));
                        middle = "=-=[" + ChatColor.DARK_PURPLE + dis + " FEMALE" + ChatColor.YELLOW + " ]=-=";
                    }
                } else {
                    volunteers.put(dis, new Sacrifice(true, true));
                    middle = "=-=[" + ChatColor.DARK_PURPLE + dis + ChatColor.YELLOW + " ]=-=";
                }

            } catch(NumberFormatException e) {
                MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, args[0] + " is an invalid digit [1-12]");
            }
        }

        Bukkit.broadcastMessage(ChatColor.YELLOW + "-=-=-=[" + ChatColor.DARK_PURPLE + "HG Reaper" + ChatColor.YELLOW + "]=-=-=-");
        TextComponent msg = new TextComponent(ChatColor.GREEN + "[CLICK HERE]" + ChatColor.WHITE + " To volunteer as tribute");
        BaseComponent[] hoverMsg = {new TextComponent(ChatColor.RED + "" + ChatColor.ITALIC + "I volunteer, I volunteer as tribute")};

        msg.setColor(ChatColor.GREEN);

        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverMsg));
        msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reap sv"));

        Bukkit.spigot().broadcast(msg);
        Bukkit.broadcastMessage(ChatColor.YELLOW + "-=-=-=-" + ChatColor.YELLOW + middle + ChatColor.YELLOW + "-=-=-=-");

        new BukkitRunnable() {

            @Override
            public void run() {
                for(Map.Entry<Integer, Sacrifice> s : volunteers.entrySet()) {
                    s.getValue().setVolunteers(s.getKey());
                }

                volunteers.clear();
            }
        }.runTaskLater(plugin, plugin.getConfig().getInt("time")*20);
    }

    private class Sacrifice {
        private Player male, female;
        private boolean addMale, addFemale;

        Sacrifice(boolean addMale, boolean addFemale) {
            male = null;
            female = null;
            this.addFemale = addFemale;
            this.addMale = addMale;
        }

        public boolean addPlayer(Player p) {
            District d;

            if((d = ReapAPI.getDistrictByPlayer(p.getUniqueId())) != null) {
                if(d.getMaleTesserae().containsKey(p.getUniqueId())) {
                    if(male == null && this.addMale) {
                        male = p;
                        return true;
                    }

                    return false;
                } else {
                    if(female == null && this.addFemale) {
                        female = p;
                        return true;
                    }

                    return false;
                }
            } else {
                return false;
            }
        }

        void setVolunteers(int district) {
            if(male != null) {
                String toRemove = "";

                for(String s : GenerateTributes.reapList) {
                    String[] sep = s.split("_");

                    if(sep[1].equals(String.valueOf(district)) && sep[2].equalsIgnoreCase("MALE")) {
                        toRemove = s;
                        break;
                    }
                }

                //For all that I know no one could be chosen
                if(!toRemove.equals("")) {
                    GenerateTributes.reapList.remove(toRemove);
                }

                GenerateTributes.reapList.add(male.getUniqueId() + "_" + district + "_" + "MALE");
            }

            if(female != null) {
                String toRemove = "";

                for(String s : GenerateTributes.reapList) {
                    String[] sep = s.split("_");

                    if(sep[1].equals(String.valueOf(district)) && sep[2].equalsIgnoreCase("FEMALE")) {
                        toRemove = s;
                        break;
                    }
                }

                //For all that I know no one could be chosen
                if(!toRemove.equals("")) {
                    GenerateTributes.reapList.remove(toRemove);
                }

                GenerateTributes.reapList.add(female.getUniqueId() + "_" + district + "_" + "FEMALE");
            }
        }
    }
}
