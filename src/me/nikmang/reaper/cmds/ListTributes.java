package me.nikmang.reaper.cmds;

import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.UUID;

/**
 * Created by ngile on 6/6/2016.
 */
@CommandInfo(description = "List Tributes that have been chosen last", usage = "[District]", aliases = {"list", "l", "tributes"}, nonPlayer = true)
public class ListTributes extends GenericCommand {


    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if(args.length < 1) {
            sender.sendMessage(ChatColor.YELLOW + "-=-=-=[" + ChatColor.DARK_PURPLE + "HG Reaper" + ChatColor.YELLOW + "]=-=-=-");
            for(String s : GenerateTributes.reapList) {
                String[] parsed = s.split("_");

                sender.sendMessage(ChatColor.DARK_PURPLE + "Player: " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(UUID.fromString(parsed[0])).getName() + ChatColor.DARK_PURPLE + " District: " + ChatColor.YELLOW + parsed[1]);
            }
            sender.sendMessage(ChatColor.YELLOW + "-=-=-=-" + "=-=-=-=-=" + ChatColor.YELLOW + "-=-=-=-");
        } else {
            try {
                int district = Integer.parseInt(args[0]);

                sender.sendMessage(ChatColor.YELLOW + "-=-=-=[" + ChatColor.DARK_PURPLE + "HG Reaper" + ChatColor.YELLOW + "]=-=-=-");
                for(String s : GenerateTributes.reapList) {
                    String[] parsed = s.split("_");

                    if(parsed[1].equals(args[0]))
                        sender.sendMessage(ChatColor.DARK_PURPLE + "Player: " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(UUID.fromString(parsed[0])).getName() + ChatColor.DARK_PURPLE + " District: " + ChatColor.YELLOW + parsed[1]);
                }
                sender.sendMessage(ChatColor.YELLOW + "-=-=-=-[" + ChatColor.DARK_PURPLE + "Dis " + district + ChatColor.YELLOW + "]-=-=-=-");
            } catch (NumberFormatException e) {
                MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Argument has to be a number!");
                return;
            }
        }
    }
}
