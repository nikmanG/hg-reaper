package me.nikmang.reaper.cmds;

import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by ngile on 6/7/2016.
 */
@CommandInfo(description = "Show tesserae for you or specific player", usage = "[Player]", aliases = {"count", "tc", "tesseraecount"}, nonPlayer = false)
public class TesseraeCount extends GenericCommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        District d;
        int tes = 0;
        OfflinePlayer pl;

        if(args.length < 1) {
            pl = (Player) sender;
        } else {
            if(!sender.hasPermission("reaper.scanother")) {
                MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Not enough permissions!");
                return;
            }

            pl = Bukkit.getOfflinePlayer(args[0]);

            if(pl  == null) {
                MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "Player does not exist!");
                return;
            }
        }

        d = ReapAPI.getDistrictByPlayer(pl.getUniqueId());

        if(d == null) {
            MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.BAD, "No District found for player!");
            return;
        }

        if(d.getMaleTesserae().containsKey(pl.getUniqueId()))
            tes = d.getMaleTesserae().get(pl.getUniqueId());
        else
            tes = d.getFemaleTesserae().get(pl.getUniqueId());

        MessageManager.getMessageManager().msg(sender, MessageManager.MessageType.INFO, pl.getName() + " Tesserae Count: " + ChatColor.GREEN + "" + tes);
    }
}
