package me.nikmang.reaper.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.api.ReapAPI.Gender;
import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import me.nikmang.reaper.handlers.MessageManager.MessageType;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 * 
 * @author Nikita
 *
 * @since 13 Apr 2016
 */
@CommandInfo(aliases = { "addplayer", "ap", "add" }, description = "Adds a player to a district and set gender", nonPlayer = true, usage = "<Player> <District> <Gender>")
public class AddPlayer extends GenericCommand {

	@Override
	public void onCommand(CommandSender sender, String[] args) {
		if(args.length < 3) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Insufficient arguments: /reap addplayer <player> <district> <Male/Female>");
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "e.g) /reap addplayer nikmanG 3 Male");
			return;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		
		if(target == null) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Player not online!");
			return;
		}
		
		int district = 0;
		
		//Implicit try-catch to return if not found
		try {
			district = Integer.parseInt(args[1]);
		} catch(NumberFormatException e) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Invalid number!");
			return;
		}
		
		if(district > 12 || district < 1) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Invalid number! (1-12)");
			return;
		}
		
		if(!target.hasPermission("reap.district" + district)) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Player does not have valid perm for this!");
			return;
		}

		if(ReapAPI.getDistrictByPlayer(target.getUniqueId()) != null) {
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Player is already in a district");
			return;
		}
		
		switch(ReapAPI.Gender.valueOf(args[2].toUpperCase())) {
		case MALE:
			ReapAPI.addPlayer(target.getUniqueId(), district, Gender.MALE);
			break;
		case FEMALE:
			ReapAPI.addPlayer(target.getUniqueId(), district, Gender.FEMALE);
			break;
		default:
			MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Invalid gender! (and no mayonnaise is not a gender)");
			return;
		}
		
		MessageManager.getMessageManager().msg(sender, MessageType.GOOD, "Added " + target.getName() + " of gender " + args[2] + " to district " + district);
	}

}
