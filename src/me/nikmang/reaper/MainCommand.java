package me.nikmang.reaper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.nikmang.reaper.cmds.*;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import me.nikmang.reaper.handlers.CommandInfo;
import me.nikmang.reaper.handlers.GenericCommand;
import me.nikmang.reaper.handlers.MessageManager;
import me.nikmang.reaper.handlers.MessageManager.MessageType;

/**
 * 
 * @author Nikita
 *
 * @since 8 Jul 2015
 */
public class MainCommand implements CommandExecutor {
	private List<GenericCommand> cmds;
	
	public MainCommand(Reaper plugin) {
		cmds = new ArrayList<>();
		
		cmds.add(new GiveTesserae(plugin));
		cmds.add(new UUIDDump());
		cmds.add(new AddPlayer());
		cmds.add(new CreateList(plugin));
		cmds.add(new GenerateTributes());
		cmds.add(new ListTributes());
		cmds.add(new ResetTesserae());
		cmds.add(new Volunteer(plugin));
        cmds.add(new SetVolunteer());

		cmds.add(new TesseraeCount());
        cmds.add(new ChangePlayer());
		cmds.add(new RemovePlayer(plugin));
		
		for(GenericCommand cmd : cmds) {
			Bukkit.getServer().getPluginManager().addPermission(new Permission("reaper." + cmd.getClass().getSimpleName()));
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (args.length == 0) {
			if(!sender.hasPermission("reaper.cmds")) {
				MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Insufficient permissions");
				return true;
			}
			
			sender.sendMessage(ChatColor.GREEN + "-=-=-=-" + ChatColor.DARK_PURPLE+ "[" + ChatColor.GOLD + "HG Reaper" + ChatColor.DARK_PURPLE + "]" + ChatColor.GREEN + "-=-=-=-");
			for (GenericCommand gcmd : cmds) {
				CommandInfo info = gcmd.getClass().getAnnotation(CommandInfo.class);
				sender.sendMessage(ChatColor.AQUA + "/reap " + StringUtils.join(info.aliases(), " ").trim() + " " + info.usage() + ChatColor.BLUE + " - " + info.description());
			}
			sender.sendMessage(ChatColor.GREEN + "-=-=-=-" + "=" + "-=-=-=-=-=" + "-" + "=-=-=-=");

			return true;
		}

		GenericCommand wanted = null;
		
		for (GenericCommand gcmd : cmds) {
			CommandInfo info = gcmd.getClass().getAnnotation(CommandInfo.class);

			for (String alias : info.aliases()) {
				if (alias.equalsIgnoreCase(args[0])) {
					wanted = gcmd;
					break;
				}
			}
		}
		
		if(wanted == null) {
			if(!sender.hasPermission("reaper.cmds")) {
				MessageManager.getMessageManager().msg(sender, MessageType.BAD, "Insufficient permissions");
				return true;
			}
			
			sender.sendMessage(ChatColor.GREEN + "-=-=-=-" + ChatColor.DARK_PURPLE+ "[" + ChatColor.GOLD + "HG Reaper" + ChatColor.DARK_PURPLE + "]" + ChatColor.GREEN + "-=-=-=-");

			for (GenericCommand gcmd : cmds) {
				CommandInfo info = gcmd.getClass().getAnnotation(CommandInfo.class);
				sender.sendMessage(ChatColor.AQUA + "/reap " + StringUtils.join(info.aliases(), " ").trim() + " " + info.usage() + ChatColor.BLUE + " - " + info.description());
			}
			
			sender.sendMessage(ChatColor.GREEN + "-=-=-=-" + "=" + "-=-=-=" + "-" + "=-=-=-=");
			return true;
		}
		
		if(!(sender instanceof Player)) {
			if(wanted.getClass().getAnnotation(CommandInfo.class).nonPlayer() == false) {
				sender.sendMessage(ChatColor.RED + "Must be a player to use this command");
				return true;
			}
		}
		
		ArrayList<String> newArgs = new ArrayList<String>();
		Collections.addAll(newArgs, args);
		newArgs.remove(0);
		args = newArgs.toArray(new String[newArgs.size()]);

		wanted.onCommand(sender, args);

		return true;
	}
}
