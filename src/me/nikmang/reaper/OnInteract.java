package me.nikmang.reaper;

import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.handlers.MessageManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class OnInteract implements Listener {

    private Material tesserae;

    public OnInteract(Material tesserae) {
        this.tesserae = tesserae;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem() != null && e.getItem().getType() == tesserae) {
                if (e.getItem().hasItemMeta()) {
                    if (e.getItem().getItemMeta().hasLore() && e.getItem().getItemMeta().getDisplayName().equals(ChatColor.RED + "[" + ChatColor.GRAY + "Tesserae" + ChatColor.RED + "]")) {
                        if(e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                            MessageManager.getMessageManager().msg(e.getPlayer(), MessageManager.MessageType.BAD, "Need to be in survival to use this");
                            return;
                        }

                        e.getItem().setAmount(e.getItem().getAmount() - 1);
                        e.getPlayer().setItemInHand(e.getItem());
                        District dis;

                        if ((dis = ReapAPI.getDistrictByPlayer(e.getPlayer().getUniqueId())) == null) {
                            return;
                        }
                        e.setCancelled(true);

                        ReapAPI.Gender g;

                        if (dis.getFemaleTesserae().containsKey(e.getPlayer().getUniqueId())) {
                            g = ReapAPI.Gender.FEMALE;
                        } else {
                            g = ReapAPI.Gender.MALE;
                        }

                        ReapAPI.addPlayer(e.getPlayer().getUniqueId(), dis.getNumber(), g);
                        MessageManager.getMessageManager().msg(e.getPlayer(), MessageManager.MessageType.GOOD, "You have added 1 to you tesserae amount.");
                    }
                }
            }
        }
    }
}
