package me.nikmang.reaper.api;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import me.nikmang.reaper.Reaper;
import org.bukkit.Bukkit;

public class ReapAPI {

	private static Map<UUID, Integer> players;
	
	public enum Gender {
		MALE,
		FEMALE
	}
	
	static {
		players = new HashMap<>();
	}
	
	private ReapAPI() {	}
	
	/**
	 * Will get an unmodifiable map of players and their corresponding District numbers
	 * 
	 * @return A map of players and corresponding districts
	 * 
	 * @deprecated this method shouldn't be used and others favoured for it
	 */
	public static Map<UUID, Integer> getPlayers() {
		return Collections.unmodifiableMap(players);
	}
	
	public static District getDistrictByPlayer(UUID uuid) {
		if(players.containsKey(uuid)) {
			return Reaper.getDistricts().get(players.get(uuid));
		}
		
		return null;
	}

	public static boolean removePlayer(UUID uuid) {
        District d = getDistrictByPlayer(uuid);

        if(d == null) {
            return false;
        }

        d.removePlayer(uuid);
        players.remove(uuid);
        return true;
    }
	
	public static void addPlayer(UUID uuid, Gender gender) {
		if(players.get(uuid) == null)
			return;
		
		addPlayer(uuid, players.get(uuid), gender);
	}
	
	public static void addPlayer(UUID uuid, int district, Gender gender) {
		addPlayer(uuid, district, gender, 1);
	}
	
	public static void addPlayer(UUID uuid, int district, Gender gender, int tesseraeAmount) {
		switch(gender) {
		case FEMALE:
			Reaper.getDistricts().get(district).addFemaleTesserae(uuid, tesseraeAmount);
			break;
		case MALE:
			Reaper.getDistricts().get(district).addMaleTesserae(uuid, tesseraeAmount);
			break;
		}
		
		players.put(uuid, district);
	}
}
