package me.nikmang.reaper.api;

import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

/**
 * 
 * @author Nikita
 *
 * @since 13 Apr 2016
 */
public class District {
	
	private Map<UUID, Integer> maleTesserae, femaleTesserae;
	private int number;
	
	public District(int number) {
		this.maleTesserae = new HashMap<>();
		this.femaleTesserae = new HashMap<>();

		this.number = number;
	}

	public Map<UUID, Integer> getMaleTesserae() {
		return Collections.unmodifiableMap(maleTesserae);
	}
	
	public Map<UUID, Integer> getFemaleTesserae() {
        return Collections.unmodifiableMap(femaleTesserae);
	}
	
	public int addMaleTesserae(UUID uuid, int addition) {
		if(!maleTesserae.containsKey(uuid)) {
			maleTesserae.put(uuid, addition);
			return addition;
		}
		
		return maleTesserae.put(uuid, maleTesserae.get(uuid) + addition);
	}
	
	public int addFemaleTesserae(UUID uuid, int addition) {
		if(!femaleTesserae.containsKey(uuid)) {
			femaleTesserae.put(uuid, addition);
			return addition;
		}
		
		return femaleTesserae.put(uuid, femaleTesserae.get(uuid) + addition);
	}

	public void removePlayer(UUID uuid) {
        maleTesserae.remove(uuid);
        femaleTesserae.remove(uuid);
    }

    @Deprecated
    public void setMaleTesserae(UUID uuid, int value) {
        maleTesserae.put(uuid, value);
    }

    @Deprecated
    public void setFemaleTesserae(UUID uuid, int value) {
        femaleTesserae.put(uuid, value);
    }
	
	public List<UUID> getMaleReapList() {
		List<UUID> reapList = new ArrayList<>();
		
		for(Entry<UUID, Integer> entry : maleTesserae.entrySet()) {
			for(int i = 0; i < entry.getValue(); i++) {
				reapList.add(entry.getKey());
			}
		}

        return reapList;
	}
	
	public List<UUID> getFemaleReapList() {
		List<UUID> reapList = new ArrayList<>();
		
		for(Entry<UUID, Integer> entry : femaleTesserae.entrySet()) {
			for(int i = 0; i < entry.getValue(); i++) {
				reapList.add(entry.getKey());
			}
		}
		
		return reapList;
	}

	@Override
    public boolean equals(Object o) {
        if(!(o instanceof District)) {
            return false;
        }

        District d = (District) o;

        return d.getMaleReapList().equals(this.getMaleReapList()) && d.getNumber() == this.getNumber();
    }

    public int getNumber() {
        return number;
    }
}
