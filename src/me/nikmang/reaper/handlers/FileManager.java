package me.nikmang.reaper.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.nikmang.reaper.Reaper;
import me.nikmang.reaper.api.District;
import me.nikmang.reaper.api.ReapAPI;
import me.nikmang.reaper.api.ReapAPI.Gender;

public class FileManager {

	private static FileManager instance;
	
	private File file;
	private FileConfiguration fc;
	
	private FileManager(Reaper plugin) {
		this.file = new File(plugin.getDataFolder(), "players.yml");
		
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		this.fc = YamlConfiguration.loadConfiguration(file);
	}
	
	//Do not need multiple instances of this
	public static FileManager getFileManager(Reaper plugin) {
		if(instance == null) {
			instance = new FileManager(plugin);
		} 
		
		return instance;
	}

	public void removePlayer(UUID uuid) {
		fc.set(uuid.toString(), null);
	}

	public void loadData() {
		//Data is saved as:
		//UUID:
		//    district: 6
		//    count: 1
		//    gender: MALE
		for(String string : fc.getKeys(false)) {			
			if(fc.getString(string + ".gender").equalsIgnoreCase("MALE")) {
				ReapAPI.addPlayer(UUID.fromString(string), fc.getInt(string + ".district"), Gender.MALE, fc.getInt(string + ".count"));
			} else if(fc.getString(string + ".gender").equalsIgnoreCase("FEMALE")) {
				ReapAPI.addPlayer(UUID.fromString(string), fc.getInt(string + ".district"), Gender.FEMALE, fc.getInt(string + ".count"));
			}
		}
	}
	
	public void saveData() {
		for(Entry<Integer, District> district : Reaper.getDistricts().entrySet()) {
			for(Entry<UUID, Integer> entry : district.getValue().getMaleTesserae().entrySet()) {
				fc.set(entry.getKey() + ".district", district.getKey());
				fc.set(entry.getKey() + ".count", entry.getValue());
				fc.set(entry.getKey() + ".gender", "MALE");
			}
			
			for(Entry<UUID, Integer> entry : district.getValue().getFemaleTesserae().entrySet()) {
				fc.set(entry.getKey() + ".district", district.getKey());
				fc.set(entry.getKey() + ".count", entry.getValue());
				fc.set(entry.getKey() + ".gender", "FEMALE");
			}
		}
		
		try {
			fc.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
