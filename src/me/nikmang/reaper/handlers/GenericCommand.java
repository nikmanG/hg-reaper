package me.nikmang.reaper.handlers;

import org.bukkit.command.CommandSender;

/**
 * 
 * @author Nikita
 * 
 * @since 23 May 2015
 */
public abstract class GenericCommand {

	public abstract void onCommand(CommandSender sender, String[] args);
}
