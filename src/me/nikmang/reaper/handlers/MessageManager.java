package me.nikmang.reaper.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

/**
 * 
 * @author Nikita
 * 
 * @since 24 May 2015
 */
public class MessageManager {
	
	private static final String PREFIX = ChatColor.DARK_PURPLE + "[" + ChatColor.YELLOW + "HG Reaper" + ChatColor.DARK_PURPLE + "] " + ChatColor.RESET;

	public enum MessageType {
		
		INFO(ChatColor.DARK_PURPLE),
		GOOD(ChatColor.GREEN),
		BAD(ChatColor.RED);
		
		private ChatColor color;
		
		MessageType(ChatColor color) {
			this.color = color;
		}
		
		public ChatColor getColor() {
			return color;
		}
	}
	
	private MessageManager() { }
	
	private static MessageManager instance = new MessageManager();
	
	public static MessageManager getMessageManager() {
		return instance;
	}
	
	public void broadcast(MessageType messageType, String msg) {
		Bukkit.getServer().broadcastMessage(PREFIX + messageType.getColor() + msg);
	}
	
	public void msg(Player p, MessageType messagetype, String msg) {
		p.sendMessage(PREFIX + messagetype.getColor() + ChatColor.translateAlternateColorCodes('&', msg));
	}

	public void msg(CommandSender sender, MessageType messagetype, String msg) {
		sender.sendMessage(PREFIX + messagetype.getColor() + ChatColor.translateAlternateColorCodes('&', msg));
	}
}